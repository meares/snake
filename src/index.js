import p5 from 'p5';
import Food from './Food';
import Snake from './Snake';

const CANVAS_WIDTH = 200;
const CANVAS_HEIGHT = 200;

const snake = new Snake();
let food = null;

new p5(_p5 => {
    _p5.setup = () => {
        _p5.createCanvas(CANVAS_WIDTH, CANVAS_HEIGHT);
        _p5.frameRate(10);
    };

    _p5.draw = () => {
        _p5.background(0);

        if(food !== null && snake.eat(food)) {
            snake.total++;
            food = null;

            console.log('eaten', snake.total);
        }

        if(food === null) {
            console.log(Math.floor(_p5.random(Math.floor(CANVAS_WIDTH / 10))));

            food = new Food(Math.floor(_p5.random(Math.floor(CANVAS_WIDTH / 10))) * 10, Math.floor(_p5.random(Math.floor(CANVAS_HEIGHT / 10))) * 10);
        }

        snake.update(_p5);
        snake.render(_p5);
        food.render(_p5);
    };

    _p5.keyPressed = event => {
        switch(event.keyCode) {
            case _p5.UP_ARROW:      snake.direction(0, -10); break;
            case _p5.DOWN_ARROW:    snake.direction(0, 10);  break;
            case _p5.RIGHT_ARROW:   snake.direction(10, 0);  break;
            case _p5.LEFT_ARROW:    snake.direction(-10, 0); break;
        }
    };
});