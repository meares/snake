export default class Food {
    constructor(x, y) {
        this.position = {};
        this.position.x = x;
        this.position.y = y;
    }

    render(p5) {
        p5.fill(255, 0, 0);
        p5.rect(this.position.x, this.position.y, 10, 10);
    }
}