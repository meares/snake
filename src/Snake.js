export default class Snake {
    constructor() {
        this.position = {};
        this.position.x = 0;
        this.position.y = 0;
        this.speed = {};
        this.speed.x = 10;
        this.speed.y = 0;
        this.total = 0;
        this.tail = [];
    }

    update(p5) {
        for(let i = 0; i < this.tail.length; i++) {
            if(this.position.x === this.tail[i].x && this.position.y === this.tail[i].y) {
                this.total = 0;
                this.tail = [];
                this.speed.x = 10;
                this.speed.y = 0;
            }
        }

        if(this.total === this.tail.length) {
            for(let i = 0; i < this.tail.length - 1; i++) {
                this.tail[i] = this.tail[i + 1];
            }
        }

        this.tail[this.total - 1] = {
            x: this.position.x,
            y: this.position.y
        };

        this.position.x = this.position.x + this.speed.x;
        this.position.y = this.position.y + this.speed.y;

        this.position.x = p5.constrain(this.position.x, 0, p5.width - 10);
        this.position.y = p5.constrain(this.position.y, 0, p5.height - 10);
    }

    render(p5) {
        p5.fill(255);

        for(let i = 0; i < this.tail.length; i++) {
            p5.rect(this.tail[i].x, this.tail[i].y, 10, 10);
        }

        p5.rect(this.position.x, this.position.y, 10, 10);
    }

    direction(x, y) {
        this.speed.x = x;
        this.speed.y = y;
    }

    eat(food) {
        return this.position.x === food.position.x && this.position.y === food.position.y;
    }
}